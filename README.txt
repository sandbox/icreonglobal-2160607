jQuery File Upload Widget

This module provides a <strong> jQuery file upload widget </strong> for the CCK file field.

By using this module, you can upload files using the standard PHP file function or NodeJS. The default upload method is set to PHP.

After installing this module, you will get an additional widget �jQuery File Upload� for selection in the CCK file field config. 

For file upload with NodeJS, you must install NodeJS server on your hosting environment. In the module configuration, you can define the NodeJS server path and upload directory. By default, it will save file in the NodeJS upload folder under public directory. 

You can also change upload directory path in nodejs.config.js file. 

Documentation: <a href='http://nodejs.org/' target='_blank'>NodeJS</a>

Related project: <a href='https://drupal.org/project/jquery_file_upload' target='_blank'>jquery_file_upload</a>

