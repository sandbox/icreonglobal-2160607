/*
 * jQuery File Upload Plugin JS Example 8.9.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, regexp: true */
/*global $, window, blueimp */
var total_count=0;
$(function() {
    'use strict';
    console.log(Drupal.settings.jquery_file_upload_widget.formid);
    var done_count = 0;
    // Initialize the jQuery File Upload widget:
    if($('#' + Drupal.settings.jquery_file_upload_widget.formid).length>0){
        console.log(Drupal.settings.jquery_file_upload_widget); 
        $('#' + Drupal.settings.jquery_file_upload_widget.formid).fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},

            url: Drupal.settings.jquery_file_upload_widget.file_upload_callback,
            acceptFileTypes: new RegExp("(\.|\/)(" + Drupal.settings.jquery_file_upload_widget.allowed_extension + ")$"),
            maxNumberOfFiles: Drupal.settings.jquery_file_upload_widget.cardinality,
            maxFileSize: Drupal.settings.jquery_file_upload_widget.max_filesize,
            done: function(e, data) {
                if (e.isDefaultPrevented()) {
                    return false;
                }
                var that = $(this).data('blueimp-fileupload') ||
                        $(this).data('fileupload'),
                        getFilesFromResponse = data.getFilesFromResponse ||
                        that.options.getFilesFromResponse,
                        files = getFilesFromResponse(data),
                        template,
                        deferred;

                if (data.context) {
                    data.context.each(function(index) {
                        var file = files[index] ||
                                {error: 'Empty file upload result'};
                        deferred = that._addFinishedDeferreds();

                        that._transition($(this)).done(
                                function() {
                                    var node = $(this);
                                    template = that._renderDownload([file])
                                            .replaceAll(node);
                                    that._forceReflow(template);
                                    that._transition(template).done(
                                            function() {
                                                data.context = $(this);
                                                $(data.jqXHR.responseJSON.files).each(function(key, value) {

                                                    var field_name = Drupal.settings.jquery_file_upload_widget.field_name,
                                                        file_url = file.url;
                                                    if(value.uri!=undefined){
                                                        file_url = value.uri;
                                                    }  
                                                    $(data.context).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][filename]' value='" + value.name + "' />");
                                                    $(data.context).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][uri]' value='" + file_url + "' />");
                                                    $(data.context).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][fid]' value='" + 0 + "' />");
                                                    $(data.context).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][display]' value='1' />");
                                                    $(data.context).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][size]' value='"+file.size+"' />");
                                                    done_count++;
                                                });

                                                that._trigger('completed', e, data);
                                                that._trigger('finished', e, data);
                                                deferred.resolve();
                                                Drupal.settings.jquery_file_upload_widget.upload_done_count++;
                                                if(Drupal.settings.jquery_file_upload_widget.uploadSubmit){
                                                    if(Drupal.settings.jquery_file_upload_widget.upload_done_count == Drupal.settings.jquery_file_upload_widget.add_count){
                                                        $('#' + Drupal.settings.jquery_file_upload_widget.formid).submit();
                                                    }
                                                }
                                            }
                                    );
                                }
                        );
                    });
                } else {
                    template = that._renderDownload(files)[
                            that.options.prependFiles ? 'prependTo' : 'appendTo'
                    ](that.options.filesContainer);
                    that._forceReflow(template);
                    deferred = that._addFinishedDeferreds();
                    that._transition(template).done(
                            function() {
                                data.context = $(this);
                                var index=0;
                                $(data.result.files).each(function(key, value) {
                                    var field_name = Drupal.settings.jquery_file_upload_widget.field_name;
                                    if(value.fid==undefined){
                                        $(data.context[index]).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][filename]' value='" + value.name + "' />");
                                        $(data.context[index]).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][uri]' value='" + value.uri + "' />");
                                        $(data.context[index]).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][fid]' value='" + 0 + "' />");
                                        $(data.context[index]).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][display]' value='1' />");
                                        $(data.context[index]).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][size]' value='"+value.size+"' />");
                                    }else{
                                       $(data.context[index]).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][fid]' value='" + value.fid + "' />"); 
                                    }
                                    $(data.context[key]).append("<input type='hidden' name='" + field_name + "[und][" + done_count + "][display]' value='1' />");
                                    done_count++;
                                    index++;
                                });
                                that._trigger('completed', e, data);
                                that._trigger('finished', e, data);
                                deferred.resolve();
                            }
                    );
                }


            }

        });

        // Enable iframe cross-domain access via redirect option:
        $('#' + Drupal.settings.jquery_file_upload_widget.formid).fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                /\/[^\/]*$/,
                '/jQuery-File-Upload-8.9.0/cors/result.html?%s'
                )
                );

        // Load existing files:
        $('#' + Drupal.settings.jquery_file_upload_widget.formid).fileupload('option', 'done')
                .call('#' + Drupal.settings.jquery_file_upload_widget.formid, $.Event('done'), {
            result: JSON.parse(Drupal.settings.jquery_file_upload_widget.existing_files)
        });

        $('#' + Drupal.settings.jquery_file_upload_widget.formid).fileupload('option', 'done')
                .call('#' + Drupal.settings.jquery_file_upload_widget.formid, $.Event('done'), {
            result: JSON.parse(Drupal.settings.jquery_file_upload_widget.uploaded_files)
        });
    }

});


$('#' + Drupal.settings.jquery_file_upload_widget.formid).bind('fileuploadsubmit', function(e,data){
    Drupal.settings.jquery_file_upload_widget.add_count++;
    
    data.formData = {
                        file_directoy: Drupal.settings.jquery_file_upload_widget.file_directoy,
                        field_name: Drupal.settings.jquery_file_upload_widget.field_name
                    };
    
});

$('#' + Drupal.settings.jquery_file_upload_widget.formid).bind('fileuploadadd', function(e,data){
   // Drupal.settings.jquery_file_upload_widget.add_count++;
});

$('#' + Drupal.settings.jquery_file_upload_widget.formid).bind('fileuploadfail', function(e,data){
    if (data.textStatus == 'abort') {
        Drupal.settings.jquery_file_upload_widget.add_count--;
    }
});

function upload_submit(){
   
    if(Drupal.settings.jquery_file_upload_widget.upload_done_count != Drupal.settings.jquery_file_upload_widget.add_count){
        Drupal.settings.jquery_file_upload_widget.uploadSubmit=true;
        jQuery('.btn-primary.start').click();
    }else{
        $('#' + Drupal.settings.jquery_file_upload_widget.formid).submit();
    }
    return true;
}