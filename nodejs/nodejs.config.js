/**
 * scheme: type of scheme ssl or non-ssl
 * port: port on nodejs service is running
 * host: file uploading host
 * publicDir: file upload public directory
 * uploadDir: file public directory
 * uploadUrl: upload url
 * @type type
 */
settings = {
  scheme: 'http',
  port: 8131,
  host: 'http://drupal.icreontech-dev.com:81/drupal/',
  debug: false,
  sslKeyPath: '',
  sslCertPath: '',
  sslCAPath: '',
  publicDir: '/var/www/html/drupal/drupal/sites/default/files',
  uploadDir:'/var/www/html/drupal/drupal/sites/default/files/nodejs_upload',
  uploadUrl: '/sites/default/files/nodejs_upload/',
};
